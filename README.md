Symfony 
========================

*Monorepo for address book solution written in Symfony3.4.

##Development

#Basic Setup:
Clone project:
```bash
git clone https://gitlab.com/marouene.lahmer/address_book.git
cd address_book
```

Install composer.phar in yor project:
```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

Install packages:
```bash
php composer.phar install
```

Run server:
```bash
php bin/console server:run
```

### Start
* Client: http://127.0.0.1:8000/book/

