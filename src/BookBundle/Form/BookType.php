<?php

namespace BookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('firstname',TextType::class, ['required' => false])
	        ->add('lastname', TextType::class, ['required' => false])
	        ->add('street', TextType::class, ['required' => false, 'attr' => array('placeholder' => 'Address')])
	        ->add('addressNumber', IntegerType::class, ['required' => false])
	        ->add('zip', TextType::class, ['required' => false])
	        ->add('city',TextType::class, ['required' => false])
	        ->add('country', TextType::class, ['required' => false])
	        ->add('phoneNumber', TextType::class, ['required' => false])
	        ->add('birthday',DateType::class, ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'required' => false, 'attr' => array('placeholder' => 'DD/MM/YYYY')])
            ->add('longitude', HiddenType::class, ['required' => false])
            ->add('latitude', HiddenType::class, ['required' => false])
            ->add('zoom', HiddenType::class, ['required' => false]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BookBundle\Entity\Book'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'book';
    }


}
