<?php

namespace BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity(repositoryClass="BookBundle\Repository\BookRepository")
 */
class Book
{


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank( message = "This field is required")
     * @ORM\Column(name="firstname", type="string", length=100, nullable=false)
     */
    private $firstname;

	/**
	 * @var string
	 * @Assert\NotBlank( message = "This field is required")
	 * @ORM\Column(name="lastname", type="string", length=100, nullable=false)
	 */
	private $lastname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="street", type="string", length=255, nullable=true)
	 */
	private $street;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="address_number", type="integer", nullable=true)
	 */
	private $addressNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="zip", type="string", length=255, nullable=true)
	 */
	private $zip;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="city", type="string", length=255, nullable=true)
	 */
	private $city;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="country", type="string", length=255, nullable=true)
	 */
	private $country;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="phone_number", type="string", length=255, nullable=true)
	 */
	private $phoneNumber;

	/**
	 * @var date
	 *
	 * @ORM\Column(name="birthday", type="date", nullable=true)
	 */
	private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="zoom", type="string", length=255, nullable=true)
     */
    private $zoom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataCreation", type="datetime", nullable=true)
     */
    private $dataCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataUpdated", type="datetime", nullable=true)
     */
    private $dataUpdated;

    /**
     * Book constructor.
     * @param \DateTime $dataCreation
     */
    public function __construct()
    {
        $this->dataCreation = $this->setDataCreation();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getFirstname()
	{
		return $this->firstname;
	}

	/**
	 * @param string $firstname
	 */
	public function setFirstname($firstname)
	{
		$this->firstname = $firstname;
	}

	/**
	 * @return string
	 */
	public function getLastname()
	{
		return $this->lastname;
	}

	/**
	 * @param string $lastname
	 */
	public function setLastname($lastname)
	{
		$this->lastname = $lastname;
	}

	/**
	 * @return string
	 */
	public function getStreet()
	{
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet($street)
	{
		$this->street = $street;
	}

	/**
	 * @return int
	 */
	public function getAddressNumber()
	{
		return $this->addressNumber;
	}

	/**
	 * @param int $addressNumber
	 */
	public function setAddressNumber($addressNumber)
	{
		$this->addressNumber = $addressNumber;
	}

	/**
	 * @return string
	 */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * @param string $zip
	 */
	public function setZip($zip)
	{
		$this->zip = $zip;
	}

	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry($country)
	{
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getPhoneNumber()
	{
		return $this->phoneNumber;
	}

	/**
	 * @param string $phoneNumber
	 */
	public function setPhoneNumber($phoneNumber)
	{
		$this->phoneNumber = $phoneNumber;
	}

	/**
	 * @return date
	 */
	public function getBirthday()
	{
		return $this->birthday;
	}

	/**
	 * @param date $birthday
	 */
	public function setBirthday($birthday)
	{
		$this->birthday = $birthday;
	}

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * @param string $zoom
     */
    public function setZoom($zoom)
    {
        $this->zoom = $zoom;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Set dataCreation
     *
     * @param \DateTime $dataCreation
     * ORM\PrePersist
     * @return Page
     */
    public function setDataCreation() {
        $this->dataCreation = new \DateTime();

        return $this;
    }

    /**
     * Get dataCreation
     *
     * @return \DateTime
     */
    public function getDataCreation() {
        return $this->dataCreation;
    }

    /**
     * Set dataUpdated
     *
     * @param \DateTime $dataUpdated
     * ORM\PreUpdate
     * @return Page
     */
    public function setDataUpdated() {
        $this->dataUpdated =  new \DateTime();

        return $this;
    }

    /**
     * Get dataUpdated
     *
     * @return \DateTime
     */
    public function getDataUpdated() {
        return $this->dataUpdated;
    }



}

